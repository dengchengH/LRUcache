package LRUcache

import (
	"container/list"
	"fmt"
)

type EvictCallback func(key interface{}, value interface{})

type LRUcache struct {
	size int
	cacheList *list.List
	cache map[interface{}]*list.Element
	onEvict EvictCallback
}

type entry struct {
	key interface{}
	value interface{}
}

// NewLRUcache is to make a new LRUcache
func NewLRUcache(size int, onEvict EvictCallback) (*LRUcache, error) {
	if size <= 0 {
		return nil, fmt.Errorf("Must provide a postive size.")
	}
	c := &LRUcache{
		size: size,
		cacheList: list.New(),
		cache: make(map[interface{}]*list.Element),
		onEvict: onEvict,
	}
	return c, nil
}

// Get value with a key
func (c *LRUcache) Get(key interface{}) (value interface{}, ok bool) {
	if ent, ok := c.cache[key]; ok {
		c.cacheList.MoveToFront(ent)
		if ent.Value.(*entry).value == nil {
			return nil, false
		}
		return ent.Value.(*entry).value, true
	}
	return
}

// Add a value to the cache
func (c *LRUcache) Add(key, value interface{}) (evicted bool) {
	if ent, ok := c.cache[key]; ok {
		c.cacheList.MoveToFront(ent)
		ent.Value.(*entry).value = value
		return false
	}

	// add new item
	ent := &entry{key, value}
	entry := c.cacheList.PushFront(ent)
	c.cache[key] = entry

	evict := c.cacheList.Len() > c.size
	// verify the size
	if evict {
		c.removeOldest()
	}
	return evict
}

func (c *LRUcache) removeOldest() {
	ent := c.cacheList.Back()
	if ent != nil {
		c.removeElement(ent)
	}
}

// removeElement is used to remove a given list element from the cache
func (c *LRUcache) removeElement(e *list.Element) {
	c.cacheList.Remove(e)
	kv := e.Value.(*entry)
	delete(c.cache, kv.key)
	if c.onEvict != nil {
		c.onEvict(kv.key, kv.value)
	}
}

// Len return the length of the cache
func (c *LRUcache) Len() int {
	return c.cacheList.Len()
}

// IsContains checks if a key is in the cache
func (c *LRUcache) IsContains(key interface{}) bool {
	if c.cache[key] != nil {
		return true
	}
	return false
}

// Remove is the function to remove the provided key from the cache
func (c *LRUcache) Remove(key interface{}) bool {
	if c.cache[key] != nil {
		ent := c.cache[key]
		c.removeElement(ent)
		return true
	}
	return false
}

// RemoveOldest remove the oldest
func (c *LRUcache) RemoveOldest() (key interface{}, value interface{}, ok bool) {
	ent := c.cacheList.Back()
	if ent != nil {
		c.removeElement(ent)
		kv := ent.Value.(*entry)
		return kv.key, kv.value, true
	}
	return nil, nil, false
}

// GetOldest return the oldest entry
func (c *LRUcache) GetOldest() (key interface{}, value interface{}, ok bool) {
	ent := c.cacheList.Back()
	if ent != nil {
		kv := ent.Value.(*entry)
		return kv.key, kv.value, true
	}
	return nil, nil, false
}

// KeyList returns a slice of the keys in the cache, from oldest to newest.
func (c *LRUcache) KeyList() []interface{} {
	keyList := make([]interface{}, len(c.cache))
	i := 0
	for ent := c.cacheList.Back(); ent != nil; ent = ent.Prev() {
		keyList[i] = ent.Value.(*entry).key
		i++
	}
	return keyList
}

// Clear is to clear the cache
func (c *LRUcache) Clear() {
	for k, v := range c.cache {
		if c.onEvict != nil {
			c.onEvict(k, v.Value.(*entry).value)
		}
		delete(c.cache, k)
	}
	c.cacheList.Init()
}