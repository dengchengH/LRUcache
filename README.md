# LRUcache

#### 介绍
一个通过 go 实现的 简单的 LRUcache 库

#### 使用方法

```
go get gitee.com/dengchengH/LRUcache
```

#### 具体功能实现

新建一个 LRUcache

```go
func NewLRUcache(size int, onEvict EvictCallback) (*LRUcache, error)
```

通过 key 值获取 value 值

```go
func Get(key interface{}) (interface{}, bool)
```

添加一个 value 值到 cache 中

```go
func Add(key, value interface{}) bool
```

返回 cache 的长度

```go
func Len() int
```

根据提供的 key 删除相应的元素

```go
func Remove(key interface{}) bool 
```

删除最旧的元素

```go
func RemoveOldest() (interface{}, interface{}, bool)
```

获取最旧的元素

```go
func GetOldest() (interface{}, interface{}, bool)
```

获取当前 cache 中的列表

```go
func KeyList() []interface{}
```

清空整个 cache

```go
func Clear()
```

